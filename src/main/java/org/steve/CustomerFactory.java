package org.steve;

public class CustomerFactory {
    public Customer create(){
        return new Customer("toto", "bateau","toto.b@google.com",
                "09900", 1998, true);
    }
}
