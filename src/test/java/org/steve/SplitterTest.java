package org.steve;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class SplitterTest {

    private Customer customer;

    @Before
    public void setUp(){
        this.customer = new CustomerFactory().create();
    }

    @Test
    public void testSplitterOnIterable() {

        this.customer.setMiddleName("jacques,henry");
        Iterable<String> iterableMiddleName = Splitter.on(",").split(customer.getMiddleName());

        assertThat(Iterables.size(iterableMiddleName),is(2));
        assertThat(Iterables.contains(iterableMiddleName, "jacques"),is(true));
        assertThat(Iterables.contains(iterableMiddleName,"henry"),is(true));
        assertThat(Iterables.contains(iterableMiddleName,"paul"), is(false));
    }

    @Test
    public void testSplitterOnMap() {

        String teamCity = "PSG-Paris|OM-Marseille";

        Map<String,String> testTeamMap = new HashMap<>();
        testTeamMap.put("PSG", "Paris");
        testTeamMap.put("OM", "Marseille");

        Splitter.MapSplitter mapSplitter1 = Splitter.on("|").withKeyValueSeparator("-");
        Map<String,String> stringMapSplit = mapSplitter1.split(teamCity);

        assertThat(testTeamMap,is(stringMapSplit));
    }
}
